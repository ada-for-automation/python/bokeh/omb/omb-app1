
def on_server_loaded(server_context):
    # If present, this function executes when the server starts.
    print("on_server_loaded :\n")

def on_server_unloaded(server_context):
    # If present, this function executes when the server shuts down.
    print("on_server_unloaded :\n")

def on_session_created(session_context):
    # If present, this function executes when the server creates a session.
    print("on_session_created :\n")

def on_session_destroyed(session_context):
    # If present, this function executes when the server closes a session.
    print("on_session_destroyed :\n")


