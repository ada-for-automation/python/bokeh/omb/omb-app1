#!/usr/bin/env python3

"""
 This Python script implements a Modbus TCP Client to Bokeh example

 Based on example :
 Modbus TestKit: Implementation of Modbus protocol in python

 (C)2009 - Luc Jean - luc.jean@gmail.com
 (C)2009 - Apidev - http://www.apidev.fr

 This is distributed under GNU LGPL license, see license.txt
 
 Run it with :
 cd OMB
 bokeh serve --show OMB-App1

"""

import struct
from math import pi

import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_tcp, hooks
import logging

from datetime import datetime
import time
import pandas as pd

from bokeh.driving import count
from bokeh.layouts import column, gridplot, row
from bokeh.plotting import curdoc, figure
from bokeh.models import ColumnDataSource, Slider, Div,RELATIVE_DATETIME_CONTEXT
from bokeh.models.tools import HoverTool

"""
Modbus TCP Client to Bokeh Main
"""

server_ip_address = "127.0.0.1"
server_port = 1502

value0_name = "LT10"
value1_name = "Slider"
value2_name = "Output0"
value3_name = "Output1"

print("=============================================\n"
      "***** Modbus TCP Client to Bokeh Main ! *****\n"
      "=============================================\n")

def on_after_recv(data):
    master, bytes_data = data
    #logger.info(bytes_data)

# The Output 0 Slider
output_slider = Slider(title ="Output 0", orientation = 'horizontal',
                       value = 0, start = -32768, end = 32767, step = 10)

def output_slider_on_change(attr, old, new):
    # print(F"Slider {attr} : old = {old}, new = {new}")
    # omb_client.execute(1, cst.WRITE_SINGLE_REGISTER, 0,
    #                    output_value = int(new),
    #                    data_format = 'h')
    omb_client.execute(slave = 1,
                       function_code = cst.WRITE_MULTIPLE_REGISTERS,
                       starting_address = 0,
                       quantity_of_x = 1,
                       output_value = [int(new)],
                       data_format = '>h')

output_slider.on_change('value', output_slider_on_change)

# The Slope 0 Slider
slope0_slider = Slider(title ="Slope 0", orientation = 'horizontal',
                       value = 0, start = -1000, end = 1000, step = 10)

def slope0_slider_on_change(attr, old, new):
    omb_client.execute(slave = 1,
                       function_code = cst.WRITE_MULTIPLE_REGISTERS,
                       starting_address = 1,
                       quantity_of_x = 1,
                       output_value = [int(new)],
                       data_format = '>h')

slope0_slider.on_change('value', slope0_slider_on_change)

# The Slope 1 Slider
slope1_slider = Slider(title ="Slope 1", orientation = 'horizontal',
                       value = 0, start = -1000, end = 1000, step = 10)

def slope1_slider_on_change(attr, old, new):
    omb_client.execute(slave = 1,
                       function_code = cst.WRITE_MULTIPLE_REGISTERS,
                       starting_address = 2,
                       quantity_of_x = 1,
                       output_value = [int(new)],
                       data_format = '>h')

slope1_slider.on_change('value', slope1_slider_on_change)

source = ColumnDataSource(dict(time = [],
                               value0 = [],
                               value1 = [],
                               value2 = [],
                               value3 = []
                         ))

p = figure(height = 500, tools = "xpan, xwheel_zoom, xbox_zoom, reset",
           x_axis_type = "datetime", x_axis_label = 'Time',
           y_axis_location = "right", y_axis_label = 'Value')

p.xaxis.formatter.context = RELATIVE_DATETIME_CONTEXT()
p.xaxis.major_label_orientation = pi / 4

hover = HoverTool(
        tooltips = [
            ("time", "@time{%F%n%H:%M:%S.%3N}"),
            (value0_name, "@value0"),
            (value1_name, "@value1"),
            (value2_name, "@value2"),
            (value3_name, "@value3"),
            ])

hover.formatters = {"@time": "datetime"}

p.line(x = 'time', y = 'value0', source = source,
       alpha = 0.3, line_width = 3, color = 'blue',
       legend_label = value0_name)

p.line(x = 'time', y = 'value1', source = source,
       alpha = 0.3, line_width = 3, color = 'green',
       legend_label = value1_name)

p.line(x = 'time', y = 'value2', source = source,
       alpha = 0.3, line_width = 3, color = 'red',
       legend_label = value2_name)

p.line(x = 'time', y = 'value3', source = source,
       alpha = 0.3, line_width = 3, color = 'brown',
       legend_label = value3_name)

p.add_tools(hover)
p.legend.location = "top_right"

@count()
def update(t):

    in_data_words = \
    omb_client.execute(slave = 1,
                       function_code = cst.READ_INPUT_REGISTERS,
                       starting_address = 0,
                       quantity_of_x = 4,
                       data_format = '>Hhhh')

    # now = datetime.now()
    now = pd.Timestamp.now()
    # print(now, "Values :", in_data_words[0], in_data_words[1],
    #                        in_data_words[2], in_data_words[3])

    new_data = dict(
        time = [now],
        value0 = [in_data_words[0]],
        value1 = [in_data_words[1]],
        value2 = [in_data_words[2]],
        value3 = [in_data_words[3]]
    )

    source.stream(new_data, 300)

    # Read back sliders values in case several views are opened and manipulated
    in_data_words = \
    omb_client.execute(slave = 1,
                       function_code = cst.READ_HOLDING_REGISTERS,
                       starting_address = 0,
                       quantity_of_x = 3,
                       data_format = '>hhh')

    output_slider.value = in_data_words[0]
    slope0_slider.value = in_data_words[1]
    slope1_slider.value = in_data_words[2]

# Layout
app_title = Div(text =
                """
                <h1>App1 trends</h1>
                """,
                width = 300,
                height = 50)

widgets = column(output_slider,
                 slope0_slider,
                 slope1_slider,
                 min_height = 200)

main_stuff = column(gridplot([[p]], toolbar_location = "left", width = 1000))

layout = column(app_title,
                widgets,
                main_stuff,
                )

curdoc().add_root(layout)

curdoc().title = "Modbus TCP Client to Bokeh"

#logger = modbus_tk.utils.create_logger("console", level=logging.DEBUG)

hooks.install_hook('modbus.Master.after_recv', on_after_recv)

try:
    omb_client = modbus_tcp.TcpMaster(host = server_ip_address,
                                      port = server_port)

    curdoc().add_periodic_callback(update, 1000)

except ConnectionRefusedError:
    #logger.error("ConnectionRefusedError")
    pass

except modbus_tk.modbus.ModbusError as exc:
    #logger.error("%s- Code=%d", exc, exc.get_exception_code())
    pass


